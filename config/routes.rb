Rails.application.routes.draw do

  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end

  post "/graphql", to: "graphql#execute"
  resources :users
  resources :posts
  
  get '/posts/:id', to: 'posts#show'

  get '/about',   to: 'static#about'

  root 'static#home'
  
end
