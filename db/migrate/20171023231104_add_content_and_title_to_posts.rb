class AddContentAndTitleToPosts < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :title, :string
    add_index  :posts, :title

    add_column :posts, :content, :text
    # It all works out in the end
  end
end
