# Blog

[![Build Status](https://travis-ci.org/J-Vaughan/blog.svg?branch=posts)](https://travis-ci.org/J-Vaughan/blog)

This is my personal blog, built using [Ruby on Rails](http://rubyonrails.org)
and [Bootstrap](http://getbootstrap.com/).

## Usage

You are free to do most anything to/with this code, subject to the terms 
outlined in the [MIT License](LICENSE.md).

## Getting started

Clone the repository (if you haven't already)

```sh
git clone https://github.com/J-Vaughan/blog.git
```

Update the dependancies

```sh
bundle update
```

Migrate the database

```sh
rails db:schema:load
```

Run the test suite and ensure that all tests are `GREEN`

```sh
rails test
```

If all goes well, start the server

```sh
rails server
```

## Expandability/Customization

Ho boy, not gonna outline this yet

#### Memes

The source code is filled with terrible, frustration-provoked quips and 
one-liners, all of them dated, and most of them already past their expiry
date, and for that, I'm sorry.
