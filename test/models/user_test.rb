require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new( name:   "Test Boi",
                      email:  "testboi@stale.meme",
                      password:               "ttooppkkeekk",
                      password_confirmation:  "ttooppkkeekk")
  end

  # it = user
  test "is valid" do
    assert @user.valid?
  end
  
  test "has name" do
    @user.name = "    "
    assert_not @user.valid?
  end

  test "has email" do
    @user.email = "    "
    assert_not @user.valid?
  end

  test "has reasonable email" do
    @user.email = "A" * 245 + "@stale.meme"
    assert_not @user.valid?
  end

  test "has reasonable name" do
    @user.name = "A" * 64
    assert_not @user.valid?
  end

  test "has unique email" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email is downcased" do
    mixed_case_email = "YoUcaNT@StAYHoMe.foReVEr" # SHUT UP MOM REEEEEEEEEEEEEE
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test "has password" do
    @user.password = @user.password_confirmation = " " * 8
    assert_not @user.valid?
  end

  test "has reasonable password" do
    @user.password = @user.password_confirmation = "A" * 7
    assert_not @user.valid?
    # Symbol
    # Capitol
    # Number
  end

  # it = validation
  test "passes valid emails" do
    valid_addresses = %w[user@example.com USER@eXAMple.COm U_s-eR@exam.ple.org
                         us.er@example.cm us+er@exam.ple]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} is valid"
    end
  end

  test "blocks invalid emails" do
    invalid_addresses = %w[user@example,com user_at_example.com us.er@example.
                           user@exam_ple.com user@ex+ple.com user@example..com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} is invalid"
    end
  end

end
