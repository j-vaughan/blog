require 'test_helper'

class PostTest < ActiveSupport::TestCase
  
  def setup
    @post = Post.new(title: "Title", content: "Content content content")
  end

  test "should be valid" do
    assert @post.valid?
  end

  test "title should be present" do
    @post.title = ""
    assert_not @post.valid?
  end

  test "content should be present" do
    @post.content = ""
    assert_not @post.valid?
  end

  test "title should not exceed 255 characters" do
    @post.title = "A" * 256
    assert_not @post.valid?
  end

  test "content should not exceed 4095 characters" do
    @post.content = "A" * 4096
    assert_not @post.valid?
  end

end
