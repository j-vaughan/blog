require 'test_helper'

class StaticControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "James Vaughan"
  end
  
  test "get home" do
    get root_url
    assert_response :success
    assert_select "title", "#{@base_title}"
  end

  test "get about" do
    get about_url
    assert_response :success
    assert_select "title", "About - #{@base_title}"
  end

end
