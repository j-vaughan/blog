require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @post = Post.new(title:   "Test Post",
                     content: ("content " * 128))
    @post.save

    @post2 = @post.dup
    @post2.save
    @post2.content = "content " * 127
    @post2.title = @post2.title + "why"
    @post2.save

    @post3 = @post.dup
    @post3.title = "Test Post 3"
    @post3.save

    @post4 = @post3.dup
    @post4.save
    @post4.title = "Tset Post 4"
    @post4.save
  end  

  test "recent posts are shown" do # This needs to be updated once I polish the
    get root_url                   # helper method to accomodate short posts (TODO:)
    assert_select "div.posts h4 a", 3
  end

  test "post ids are in binary" do
    get post_url(@post.id.to_s(2))
    assert_response :success
    assert_select "div.posts h4", "#{@post.title}"
    assert_select "div.posts div.row small.text-muted", "Id #{@post.id.to_s(2)}"
  end

  test "date edited should only show when necessary" do
    get root_url
    assert_select "div.posts div.row small.col-5", 4
    assert_select "div.posts div.row small.col-10", 1
  end

  # Would've tested the truncating system, but it needs an overhaul anyways

end
