class Post < ApplicationRecord
    validates :title,   presence: true, length: { maximum: 255  }
    validates :content, presence: true, length: { maximum: 4095 }
end
