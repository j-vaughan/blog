class User < ApplicationRecord
  SHORT_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  before_save { self.email = email.downcase }

  validates :name,  presence: true,
                    length: { maximum: 63  }
  validates :email, presence: true,
                    length: { maximum: 255 },
                    format: { with: SHORT_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }

  has_secure_password

  validates :password,  presence: true,
                        length: { minimum: 8 }
end
