class PostsController < ApplicationController

  def new
    
  end

  def index
    @posts = Post.paginate(page: params[:page], per_page: 10)
  end
  
  def show
    @post = Post.find(params[:id].to_i(2))
  end

end
