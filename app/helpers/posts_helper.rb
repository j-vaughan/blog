module PostsHelper
  SHORT_DATE_REGEX = /\d{4}-\d{2}-\d{2} \d{2}:\d{2}/

  def short_date(long_date)
    SHORT_DATE_REGEX.match(long_date.to_s)
  end

  def short_post(long_post)
    if !long_post.nil?
      long_post.truncate(1024, seperator: ' ')
    end
  end

  def shorter_post(long_post)
    if !long_post.nil?
      long_post.truncate(512, seperator: ' ')
    end
  end
  
end