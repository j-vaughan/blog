module ApplicationHelper
    def make_title(page_title = '')
        if !page_title.empty?
            page_title + " - "
        else
            page_title
        end
    end
end
